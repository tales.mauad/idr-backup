<?php
/**
 * Template Name: Página de contato
 *
 * @package WordPress
 * @subpackage IDR Consultoria
 */

/* Advanced Custom Fields */
/* Texto Introdutório */
$texto_introdutorio         = get_field('texto_introdutorio');
/* Endereço */
$box_endereco               = get_field('box_endereco');

?>
<?php get_header(); ?>

    <div id="container" class="container">
        <div id="faixa-interna">
            <div>Contato</div>
        </div>
        <div id="breadcrumb">
            idr > contato
        </div>

        <div id="content">
            <div class="leadtext">
                <p><?php echo $texto_introdutorio; ?></p>
            </div>

            <?php echo do_shortcode( '[contact-form-7 id="410" title="Contato"]' ); ?>
            <div class="row">
                <div class="col-sm-8">
                <?php echo do_shortcode('[wpgmza id="1"]'); ?>    
                </div>
                <div class="col-sm-4 tabelar">
                    <div class="tabcel">
                        <?php echo $box_endereco; ?>
                    </div>
                </div>
            </div>
        </div><!-- #content -->

        <div id="faq-sidebar">
            <div class="tit-submenu">Leia também</div>
            <ul>
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                );
                $the_query = new WP_Query( $args );
                $i = 1;

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                ?>
                <li>
                    <a href="<?php the_permalink(); ?>" class="link-img">
                        <?php
                        if ( has_post_thumbnail() ) {
                            the_post_thumbnail( 'destaque_lateral' );
                        }
                        ?>
                    </a>
                    <a href="<?php the_permalink(); ?>" class="categoria">
                        <?php
                        // Obter as categorias
                        $categories = get_the_category();
                        // Verificar se existem categorias
                        if ( ! empty( $categories ) ) {
                            // Obter a primeira categoria
                            $categorie = current($categories);
                            echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                        }
                        ?>
                    </a>
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    <div class="clearfix"></div>
                    <a href="<?php the_permalink(); ?>" class="link-ler-mais">Leia mais</a>
                </li>
                <?php $i++; endwhile; endif; wp_reset_postdata(); ?>
            </ul>

            <a href="<?php echo get_site_url(); ?>/trabalhe-conosco">
                <div class="text-center">
                    <h3 class="cta-fale">TRABALHE CONOSCO</h3>
                </div><!-- cta-fale -->
            </a>
                
        </div>
    </div><!-- #container -->
<?php get_footer(); ?>