<?php get_header(); 
/*
* Template Name: Página Erro
*/
$titulo_erro        = get_field('titulo_erro');
?>

    <div id="faixa-interna">
        <div class="container">Erro</div>
    </div>
    <div id="breadcrumb">
        <div class="container">idr > erro</div>
    </div>
    <div id="container" class="container">

        <div id="content" class="errorpage">
            <h1 class="text-center mobileerrotitulo" style="padding-bottom:50px;">O que você procura?</h1>
                <div class="download-item error-item">
                    <div class="row">
                        <div class="col-sm-3 thankimage">
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/error1.png">
                        </div><!-- col2 -->
                        <div class="col-sm-9 textthankpart classmobile">
                            <a class="classerror" href="<?php echo get_site_url(); ?>/financiamento">Preciso de empréstimo para minha empresa</a>
                        </div><!-- col09 -->
                    </div><!-- row -->
                </div><!-- download-item -->
                <div class="download-item error-item">
                    <div class="row">
                        <div class="col-sm-3 thankimage">
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/error2.png">
                        </div><!-- col2 -->
                        <div class="col-sm-9 textthankpart classmobile">
                            <a class="classerror" href="<?php echo get_site_url(); ?>/fundo-perdido"><p>Tenho uma tecnologia e quero um recurso a fundo perdido para desenvolvê-la</p></a>
                        </div><!-- col09 -->
                    </div><!-- row -->
                </div><!-- download-item -->
                <div class="download-item error-item">
                    <div class="row">
                        <div class="col-sm-3 thankimage">
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/error3.png">
                        </div><!-- col2 -->
                        <div class="col-sm-9 textthankpart classmobile">
                            <a class="classerror" href="<?php echo get_site_url(); ?>/fusoes-e-aquisicoes">Quero assessoria para vender parte da minha empresa</a>
                        </div><!-- col09 -->
                    </div><!-- row -->
                </div><!-- download-item -->
                <div class="download-item error-item" >
                    <div class="row">
                        <div class="col-sm-3 thankimage">
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/error4.png">
                        </div><!-- col2 -->
                        <div class="col-sm-9 textthankpart classmobile">
                            <a class="classerror" href="<?php echo get_site_url(); ?>/lei-do-bem">Gostaria de saber mais sobre incentivos fiscais e lei do bem</a>
                        </div><!-- col09 -->
                    </div><!-- row -->
                </div><!-- download-item -->
        

            <div id="fale-consultor" style="margin-top: 75px;">
                <div class="tit1">Fale com um consultor</div>
                <div class="tit2">Deixe seu telefone e ligaremos em breve</div>
                <form action="javascript:;">
                    <input type="text" id="tel" name="tel" placeholder="Telefone" />
                    <input id="submit" type="submit" value="Enviar" />
                </form>
            </div>

        </div><!-- #content -->
        <div id="faq-sidebar" class="errorsidebar">
            <div class="tit-submenu">Leia também</div>
            <ul>
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                );
                $the_query = new WP_Query( $args );
                $i = 1;

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                ?>
                <li>
                    <a href="<?php the_permalink(); ?>" class="link-img">
                        <?php
                        if ( has_post_thumbnail() ) {
                            the_post_thumbnail( 'destaque_lateral' );
                        }
                        ?>
                    </a>
                    <a href="<?php the_permalink(); ?>" class="categoria">
                        <?php
                        // Obter as categorias
                        $categories = get_the_category();
                        // Verificar se existem categorias
                        if ( ! empty( $categories ) ) {
                            // Obter a primeira categoria
                            $categorie = current($categories);
                            echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                        }
                        ?>
                    </a>
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    <div class="clearfix"></div>
                    <a href="<?php the_permalink(); ?>" class="link-ler-mais">Leia mais</a>
                </li>
                <?php $i++; endwhile; endif; wp_reset_postdata(); ?>
            </ul>

            <a href="<?php echo get_site_url(); ?>/trabalhe-conosco">
                <div class="text-center">
                    <h3 class="cta-fale">TRABALHE CONOSCO</h3>
                </div><!-- cta-fale -->
            </a>
                
        </div>


    </div><!-- #container -->
 
<?php get_footer(); ?>