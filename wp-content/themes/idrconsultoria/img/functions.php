<?php
 
// Make theme available for translation
// Translations can be filed in the /languages/ directory
load_theme_textdomain( 'your-theme', TEMPLATEPATH . '/languages' );
 
$locale = get_locale();
$locale_file = TEMPLATEPATH . "/languages/$locale.php";
if ( is_readable($locale_file) )
        require_once($locale_file);
 
// Get the page number
function get_page_number() {
    if (get_query_var('paged')) {
        print ' | ' . __( 'Page ' , 'your-theme') . get_query_var('paged');
    }
} // end get_page_number
 
// For category lists on category archives: Returns other categories except the current one (redundant)
function cats_meow($glue) {
        $current_cat = single_cat_title( '', false );
        $separator = "\n";
        $cats = explode( $separator, get_the_category_list($separator) );
        foreach ( $cats as $i => $str ) {
                if ( strstr( $str, ">$current_cat<" ) ) {
                        unset($cats[$i]);
                        break;
                }
        }
        if ( empty($cats) )
                return false;
 
        return trim(join( $glue, $cats ));
} // end cats_meow
 
// For tag lists on tag archives: Returns other tags except the current one (redundant)
function tag_ur_it($glue) {
        $current_tag = single_tag_title( '', '',  false );
        $separator = "\n";
        $tags = explode( $separator, get_the_tag_list( "", "$separator", "" ) );
        foreach ( $tags as $i => $str ) {
                if ( strstr( $str, ">$current_tag<" ) ) {
                        unset($tags[$i]);
                        break;
                }
        }
        if ( empty($tags) )
                return false;
 
        return trim(join( $glue, $tags ));
} // end tag_ur_it
 
// Register widgetized areas
function theme_widgets_init() {
        // Area 1
  register_sidebar( array (
  'name' => 'Primary Widget Area',
  'id' => 'primary_widget_area',
  'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
  'after_widget' => "</li>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
  ) );
 
        // Area 2
  register_sidebar( array (
  'name' => 'Secondary Widget Area',
  'id' => 'secondary_widget_area',
  'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
  'after_widget' => "</li>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
  ) );
} // end theme_widgets_init
 
add_action( 'init', 'theme_widgets_init' );
 
// Pre-set Widgets
$preset_widgets = array (
        'primary_widget_area'  => array( 'search', 'pages', 'categories', 'archives' ),
        'secondary_widget_area'  => array( 'links', 'meta' )
);
if ( !isset( $_GET['activated'] ) ) {
        update_option( 'sidebars_widgets', $preset_widgets );
}
// update_option( 'sidebars_widgets', NULL );
 
// Check for static widgets in widget-ready areas
function is_sidebar_active( $index ){
  global $wp_registered_sidebars;
 
  $widgetcolums = wp_get_sidebars_widgets();
 
  if ($widgetcolums[$index]) return true;
 
        return false;
} // end is_sidebar_active
 
// Produces an avatar image with the hCard-compliant photo class
function commenter_link() {
        $commenter = get_comment_author_link();
        if ( ereg( '<a[^>]* class=[^>]+>', $commenter ) ) {
                $commenter = ereg_replace( '(<a[^>]* class=[\'"]?)', '\\1url ' , $commenter );
        } else {
                $commenter = ereg_replace( '(<a )/', '\\1class="url "' , $commenter );
        }
        $avatar_email = get_comment_author_email();
        $avatar = str_replace( "class='avatar", "class='photo avatar", get_avatar( $avatar_email, 80 ) );
        echo $avatar . ' <span class="fn n">' . $commenter . '</span>';
} // end commenter_link
 
// Custom callback to list comments in the your-theme style
function custom_comments($comment, $args, $depth) {
  $GLOBALS['comment'] = $comment;
        $GLOBALS['comment_depth'] = $depth;
  ?>
        <li id="comment-<?php comment_ID() ?>" <?php comment_class() ?>>
                <div class="comment-author vcard"><?php commenter_link() ?></div>
                <div class="comment-meta"><?php printf(__('Posted %1$s at %2$s <span class="meta-sep">|</span> <a href="%3$s" title="Permalink to this comment">Permalink</a>', 'your-theme'),
                                        get_comment_date(),
                                        get_comment_time(),
                                        '#comment-' . get_comment_ID() );
                                        edit_comment_link(__('Edit', 'your-theme'), ' <span class="meta-sep">|</span> <span class="edit-link">', '</span>'); ?></div>
  <?php if ($comment->comment_approved == '0') _e("\t\t\t\t\t<span class='unapproved'>Your comment is awaiting moderation.</span>\n", 'your-theme') ?>
          <div class="comment-content">
                <?php comment_text() ?>
                </div>
                <?php // echo the comment reply link
                        if($args['type'] == 'all' || get_comment_type() == 'comment') :
                                comment_reply_link(array_merge($args, array(
                                        'reply_text' => __('Reply','your-theme'),
                                        'login_text' => __('Log in to reply.','your-theme'),
                                        'depth' => $depth,
                                        'before' => '<div class="comment-reply-link">',
                                        'after' => '</div>'
                                )));
                        endif;
                ?>
<?php } // end custom_comments
 
// Custom callback to list pings
function custom_pings($comment, $args, $depth) {
       $GLOBALS['comment'] = $comment;
        ?>
                <li id="comment-<?php comment_ID() ?>" <?php comment_class() ?>>
                        <div class="comment-author"><?php printf(__('By %1$s on %2$s at %3$s', 'your-theme'),
                                        get_comment_author_link(),
                                        get_comment_date(),
                                        get_comment_time() );
                                        edit_comment_link(__('Edit', 'your-theme'), ' <span class="meta-sep">|</span> <span class="edit-link">', '</span>'); ?></div>
    <?php if ($comment->comment_approved == '0') _e('\t\t\t\t\t<span class="unapproved">Your trackback is awaiting moderation.</span>\n', 'your-theme') ?>
            <div class="comment-content">
                        <?php comment_text() ?>
                        </div>
<?php } // end custom_pings




// Disable Editors Globally
define('DISALLOW_FILE_EDIT', true );

/**
 * Remover a barra de admin
 */
function my_function_admin_bar(){
    return false;
}
add_filter( "show_admin_bar" , "my_function_admin_bar");

/**
 * Permitir menu no tema
 */
if ( function_exists( "register_nav_menu" ) ) {
    register_nav_menu( "mainMenu", "Menu principal" );
}


add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 470, 335 );

// Imagem de destaque na home
add_image_size( 'dicas_tutoriais', 436, 138, true );
add_image_size( 'destaque_home', 310, 220, true );
add_image_size( 'destaque_lateral', 115, 85, true );


/**
 * Criar um menu personalizado
 */
function view_menu_principal(){
    wp_nav_menu(
        array(
            "menu" => "mainMenu",
            "container" => "div",
            "container_id" => "main-menu-container",
            "menu_class" => "main-menu",
            "menu_id" => "main-menu"
        )
    );
}

/**
 * Custom Post Type FAQ
 */
add_action( 'init', 'create_posttype' );
function create_posttype() {
    register_post_type( 'faq',
        array(
            'labels' => array(
                'name' => __( 'Faq' ),
                'singular_name' => __( 'Faq' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'faq'),
            'supports' => array( 'title', 'editor', 'thumbnail' ),
            'taxonomies' => array( 'category' )
        )
    );
}
/**
 * Custom Post Type CURSOS
 */
add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'cursos',
    array(
        'labels' => array(
        'name' => __( 'Cursos' ),
        'singular_name' => __( 'Curso' )
    ),
    'public' => true,
    'has_archive' => true,
    'rewrite' => array('slug' => 'curso'),
    'supports' => array( 'title', 'editor', 'thumbnail' ),
    'taxonomies' => array( 'category' )
    )
  );
}

/**
 * Remove boxes from dashboard
 */
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
function remove_dashboard_widgets() {
    global $wp_meta_boxes;
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
}

/**
 * Remove itens from menu
add_action( 'admin_menu', 'remove_menus' );
function remove_menus() {
    remove_menu_page( 'edit-comments.php' );          //Comments
    remove_menu_page( 'tools.php' );                  //Tools
    remove_menu_page( 'themes.php' );                 //Appearance
    remove_menu_page( 'options-general.php' );        //Settings
}
 */

/**
 * Add custom meta box
 */
function custom_fields_meta_box() {
    add_meta_box(
        'your_fields_meta_box', // $id
        'Destaque na home', // $title
        'show_custom_fields_meta_box', // $callback
        'post', // $screen
        'normal', // $context
        'high' // $priority
    );
    // add_meta_box(
    //     'your_fields_meta_box', // $id
    //     'Link Curso', // $title
    //     'show_custom_fields_meta_box', // $callback
    //     'post', // $screen
    //     'normal', // $context
    //     'high' // $priority
    // );
}
add_action( 'add_meta_boxes', 'custom_fields_meta_box' );

/**
 * Show custom fields
 */
function show_custom_fields_meta_box() {
    global $post;
    // Array com custom fields
    $meta = get_post_meta( $post->ID, 'custom_fields', true );

    // Generate and returns a nonce for security purposes
    $nonce = wp_create_nonce( basename(__FILE__) );

$custom_fields = <<<CUSTOM_FIELDS
    <p>
        <div><label for="custom_fields[texto_destaque]">Texto destaque</label></div>
        <textarea name="custom_fields[texto_destaque]" id="custom_fields[texto_destaque]" rows="5" cols="30" style="width:100%; margin-top:10px;">{$meta['texto_destaque']}</textarea>
        <input type="hidden" name="custom_meta_box_nonce" value="{$nonce}">
    </p>

CUSTOM_FIELDS;

    echo $custom_fields;

}

/**
 * Save custom fields
 */
function save_custom_fields_meta( $post_id ) {
    // verify nonce
    if ( !wp_verify_nonce( $_POST['custom_meta_box_nonce'], basename(__FILE__) ) ) {
        return $post_id;
    }
    // check autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return $post_id;
    }
    // check permissions
    if ( 'page' === $_POST['post_type'] ) {
        if ( !current_user_can( 'edit_page', $post_id ) ) {
            return $post_id;
        } elseif ( !current_user_can( 'edit_post', $post_id ) ) {
            return $post_id;
        }
    }

    $old = get_post_meta( $post_id, 'custom_fields', true );
    $new = $_POST['custom_fields'];

    if ( $new && $new !== $old ) {
        update_post_meta( $post_id, 'custom_fields', $new );
    } elseif ( '' === $new && $old ) {
        delete_post_meta( $post_id, 'custom_fields', $old );
    }
}
add_action( 'save_post', 'save_custom_fields_meta' );