<?php
/**
 * Template Name: Página de Cursos
 *
 * @package WordPress
 * @subpackage IDR Consultoria
 */

/* Advanced Custom Fields */
/* Banner Principal */
$titulo_esquerda        = get_field('titulo_esquerda');
$texto_direita          = get_field('texto_direita');

/* Região de Baixo */
$titulo_embaixo         = get_field('titulo_embaixo');
$texto_embaixo          = get_field('texto_embaixo');

?>
<?php get_header(); ?>

    <div id="container" class="container fundobranco">
        <div id="faixa-interna">
            <div>Cursos</div>
        </div>
        <div id="breadcrumb">
            idr > <?php the_title(); ?>
        </div>
        <div class="img-sec" >
            <div class="row">
                <div class="col-sm-6 pull-left">
                    <h2><?php echo $titulo_esquerda ?></h2>
                </div><!-- col-sm-6 -->
                <div class="col-sm-4 pull-right coll-sm-offset-2">
                    <p class="lead"><?php echo $texto_direita ?></p>
                    <a href="<?php echo get_site_url(); ?>/contato"><img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/ti.png" /></a>
                </div><!-- col-sm-6 -->
            </div><!-- row -->
        </div><!-- img-sec -->
        <?php
            $args = array(
                'post_type' => 'cursos',
                'posts_per_page' => 8
            );
            $the_query = new WP_Query( $args );
        ?>
        <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); $meta = get_post_meta( $post->ID, 'custom_fields', true ); ?>
        <div id="curso-content">
            <div class="container">
                <div class="row gestao-inov">
                    <div class="col-sm-9 pull-left">
                        <h2><?php the_title(); ?></h2>
                    </div><!-- col-sm-6 -->
                    <div class="col-sm-3 text-right">
                        <a href="<?php echo get_site_url();?>/contato"><button class="btn btn-lg btn-saiba">SAIBA MAIS</button></a>
                    </div><!-- col-sm-6 -->
                </div><!-- row -->
                <div class="texto-cursos row">
                    <div class="col-sm-9">
                        <p class="lead ementa" style="font-size:20px;">Ementa</p>
                        <p class="negrito lead"><?php the_content(); ?></p>
                    </div>
                </div><!-- texto-cursos -->
            </div><!-- container -->
        </div><!-- #content -->
        <?php endwhile; else : ?>
        <?php endif; wp_reset_postdata(); ?>
    </div><!-- #container -->

<?php get_footer(); ?>