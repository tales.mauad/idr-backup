    </div><!-- #main -->
    <?php 

    // AVANCED CUSTOM FIELDS
    $whatsapp               = get_field('whatsapp');
    $telefone               = get_field('telefone');
    $skype                  = get_field('skype');
    $email                  = get_field('email');
    $linkedin               = get_field('linkedin');
    $endereco               = get_field('endereco');


    ?>
    <div id="footer">
        <div id="bg-newsletter"></div>
        <div class="container">
            <div id="contato">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="<?php echo get_site_url(); ?>/contato"><h3>CONTATO</h3></a>
                        <div class="hr-border"></div>
                    </div>
                </div>
                <div class="contato-itens">
                    <div class="contato-item">
                        <div class="contato-ico">
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/contato-ico-whatsapp.png" />
                        </div>
                        <div class="contato-label">whatsapp</div>
                        <div class="contato-value"><?php

                            if (!empty($whatsapp)) {
                                echo $whatsapp;
                            }
                            else {
                                echo "(11) 94700-0004";
                            } ?></div>
                    </div>
                    <div class="contato-item">
                        <div class="contato-ico">
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/contato-ico-telefone.png" />
                        </div>
                        <div class="contato-label">telefone</div>
                        <div class="contato-value"><?php

                            if (!empty($telefone)) {
                                echo $telefone;
                            }
                            else {
                                echo "(11) 5087-8957";
                            } ?></div>
                    </div>
                    <div class="contato-item">
                        <div class="contato-ico">
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/contato-ico-skype.png" />
                        </div>
                        <div class="contato-label">skype</div>
                        <div class="contato-value"><?php

                            if (!empty($skype)) {
                                echo $skype;
                            }
                            else {
                                echo "contatoidr";
                            } ?></div>
                    </div>
                    <div class="contato-item">
                        <div class="contato-ico">
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/contato-ico-email.png" />
                        </div>
                        <div class="contato-label">e-mail</div>
                        <div class="contato-value"><?php

                            if (!empty($email)) {
                                echo $email;
                            }
                            else {
                                echo "idr@idrconsultoria.com.br";
                            } ?></div>
                    </div>
                    <a href="https://www.linkedin.com/company-beta/2474371/" target="_blank" ><div class="contato-item">
                        <div class="contato-ico">
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/contato-ico-linkedin.png" />
                        </div>
                        <div class="contato-label">linkedin</div>
                        <div class="contato-value"><?php

                            if (!empty($linkedin)) {
                                echo $linkedin;
                            }
                            else {
                                echo "idr";
                            } ?></div>
                    </div></a>
                    <div id="endereco">
                        <div class="contato-ico">
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/contato-ico-endereco.png" />
                        </div>
                        <div>Rua Vergueiro,2087,1º Andar<br />Vila Mariana - São Paulo - SP</div>
                    </div>
                </div>
            </div>
            <div id="faq">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="<?php echo get_site_url(); ?>/faq/como-conseguir-um-financiamento-para-inovacao-em-sc"><h3>FAQ</h3></a>
                        <div class="hr-border"></div>
                    </div>
                </div>

                <div id="accordion">
                    <?php
                        $args = array(
                            'post_type' => 'faq',
                            'orderby' => 'rand',
                            'posts_per_page' => 8,
                            'cat' => array(
                                3, // Financiamento
                                4, // Fundo Perdido
                                5, // Lei do Bem
                                6, // M&A
                            )
                        );
                        $the_query = new WP_Query( $args );
                        $i = 1;
                    ?>
                    <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="accordion-item">
                            <h4 class="accordion-toggle">
                                <span><?php echo $i++; ?>.</span>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
                            </h4>
                            <?php if(false): ?>
                            <div class="accordion-content">
                            <p>Cras malesuada ultrices augue molestie risus. Cras malesuada ultrices augue molestie risus.Cras malesuada ultrices augue molestie risus.Cras malesuada ultrices augue molestie risus.Cras malesuada ultrices augue molestie risus.Cras malesuada ultrices augue molestie risus.Cras malesuada ultrices augue molestie risus.</p>
                            </div>
                            <?php endif ?>
                        </div>
                    <?php endwhile; else : ?>
                        <div class="accordion-item">
                            <h4 class="accordion-toggle">
                                <span>1.</span>
                                <a href="javascript:;">Nenhum item encontrado</a>
                            </h4>
                        </div>
                    <?php endif; wp_reset_postdata(); ?>

                </div>

            </div>
            <div id="newsletter">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>NEWSLETTER</h3>
                        <div class="hr-border"></div>
                    </div>
                </div>
                    <div id="bg-white">
                        <div id="form-newsletter">
                            <p>Digite seu email abaixo e receba<br />nosso conteúdo gratuito</p>
                        </div>
                        <?php echo do_shortcode( '[contact-form-7 id="445" title="Newsletter"]' ); ?>
                <div id="redes-sociais">
                    <a target="_blank" href="https://www.facebook.com/idrconsultoria/">
                        <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/ico-facebook.png" />
                    </a>
                    <a target="_blank" href="https://www.linkedin.com/company-beta/2474371/">
                        <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/ico-linkedin.png" />
                    </a>
                    <a target="_blank" href="https://twitter.com/idr_consultoria">
                        <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/ico-twitter.png" />
                    </a>
                    <a target="_blank" href="https://plus.google.com/u/0/111578744684943612015">
                        <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/ico-google-plus.png" />
                    </a>
                </div>
            </div>
        </div>
        <div id="bg-quem-somos">
            <div id="bg-chat-consultor"></div>
            <div class="container">
                <div id="quem-somos-container">
                    <a id="logo-rodape" href="<?php echo get_site_url(); ?>">
                        <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/logo-rodape.png" />
                    </a>
                    <div id="quem-somos">
                        <!-- Avanced Custom Fields - Rodapé -->
                        <?php $quem_somos       = get_field('quem_somos') ?>
                        <h4>Quem somos</h4>
                        <p><?php

                            if (!empty($quem_somos)) {
                                echo $quem_somos;
                            }
                            else {
                                echo "A idr é uma consultoria especializada em auxiliar empresas a obter recursos a fundo perdido ou financiamentos com juros reduzidos, usar incentivos fiscais ou realizar operações de M&A (Private Equity ou Venture Capital). Assim, ajudamos nossos clientes a conseguir empréstimos para projetos de expansão, capital de giro ou inovação, a aprovar projetos de subvenção para P&D, usar a Lei do Bem ou mesmo receber investimentos privados.";
                            }


                        ?></p>
                    </div>
                    <a id="voltar-topo" href="#header">
                        <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/voltar-topo.png" />
                    </a>
                </div>
                <div id="chat-consultor-container">
                    <a id="chat-consultor" href="<?php echo get_site_url(); ?>/contato">
                        <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/botao-chat.png" />
                    </a>
                </div>
            </div>
        </div>

    </div><!-- #footer -->


</div><!-- #wrapper -->

<script src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/js/jquery.min.js"></script>
<script src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/js/jquery.validate.min.js"></script>
<script src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/js/bootstrap.min.js"></script>
<script src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/js/simple-tabs.js"></script>
<script src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/js/scripts.js"></script>
<script src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/js/slicknav.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>

<script>
document.addEventListener( 'wpcf7mailsent', function( event ) {
    location = 'http://idrconsultoria.com.br/obrigado';
}, false );
</script>
<script src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/js/numberanimation/numberanimation.js"></script>
<script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/8f65a7d2-be96-4c8d-bd5c-9a3719a31eeb-loader.js"></script>
<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-68024111', 'auto');
ga('send', 'pageview');
</script>
<!-- End Google Analytics -->
<?php wp_footer(); ?>

</body>
</html>
