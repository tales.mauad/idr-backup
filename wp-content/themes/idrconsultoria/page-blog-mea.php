<?php
/**
 * Template Name: Página de blog mea
 *
 * @package WordPress
 * @subpackage IDR Consultoria
 */
?>
<?php get_header(); ?>

    <div id="faixa-interna">
        <div class="container">Blog</div>
    </div>
    <div id="breadcrumb">
        <div class="container">idr > blog</div>
    </div>
    <div id="container" class="container page-template-page-blog">
        <div id="contentblog">
            <div id="content">

                <!-- #categorias -->
                <div id="categorias" class="container">
                    <div id="escolha">
                        <div id="escolha-label">ESCOLHA SEU ASSUNTO</div>
                        <ul class="simple-tabs" id="escolha-categorias">
                            <li class=""><a href="<?php echo get_site_url(); ?>/blog">Financiamento</a><span class="divisor"></span></li>
                            <li class=""><a href="<?php echo get_site_url(); ?>/blog-fundo-perdido">Fundo Perdido</a><span class="divisor"></span></li>
                            <li class="active"><a href="<?php echo get_site_url(); ?>/blog-mea">M&amp;A</a><span class="divisor"></span></li>
                            <li class=""><a href="<?php echo get_site_url(); ?>/blog-lei-do-bem">Lei do Bem</a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>


                    <!-- .tabe-page -->
                    <div id="categoria-3" class="tab-page active-page">
                        <?php
                        $args = array(
                            'post_type' => 'post',
                            'posts_per_page' => 10,
                            // cat6 -> M&A
                            'cat' => 6,
                            'post__not_in' => $not_in,
                            'paged' => $paged
                        );
                        $the_query = new WP_Query( $args );
                        $i = 1;

                        if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                            $meta = get_post_meta( $post->ID, 'custom_fields', true );
                        ?>
                        <!-- .cat-item -->
                        <div class="cat-item">
                            <a href="<?php the_permalink(); ?>">
                            <?php
                            if ( has_post_thumbnail() ) {
                                the_post_thumbnail( 'destaque_home' );
                            } else {
                            ?>
                            <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-1.png" />
                            <?php
                            }
                            ?>
                            </a>
                            <div class="cat-item-text">
                                <div class="categoria">
                                    <?php
                                    // Obter as categorias
                                    $categories = get_the_category();
                                    // Verificar se existem categorias
                                    if ( ! empty( $categories ) ) {
                                        // Obter a primeira categoria
                                        $categorie = current($categories);
                                        echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                                    }
                                    ?>
                                </div>
                                <div class="titulo">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </div>
                                <div class="texto"><?php echo $meta['texto_destaque']; ?></div>
                                <div class="link"><a href="<?php the_permalink(); ?>">LEIA MAIS</a></div>
                            </div>
                        </div>
                        <!-- /.cat-item -->
                        <?php $i++; endwhile; endif; ?>
                        <?php wp_pagenavi(array('query' => $the_query)); ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                    <!-- /.tabe-page -->
                </div>
                <!-- /#categorias -->

                <div id="fale-consultor">
                    <div class="tit1">Fale com um consultor</div>
                    <div class="tit2">Deixe seu telefone e ligaremos em breve</div>
                    <form action="javascript:;">
                        <input type="text" id="tel" name="tel" placeholder="Telefone" />
                        <input id="submit" type="submit" value="Enviar" />
                    </form>
                </div>
            </div><!-- #content -->
        </div><!-- contentblog -->
    </div><!-- #container -->

<?php get_footer(); ?>
