<?php 
 //     Template Name: Página de agradecimento

$titulo_principal       = get_field('titulo_principal');
$titulo_menor           = get_field('titulo_menor');


?>
<?php get_header(); ?>

<!--     <div id="faixa-interna">
        <div class="container"><?php the_title(); ?></div>
    </div>
    <div id="breadcrumb">
        <div class="container">idr > obrigado</div>
    </div> -->
    <div id="container" class="container">

        <div id="content" class="text-center thankcontent pagobrigado">

            <h1><?php echo $titulo_principal ?></h1>

            <p class="lead"><?php echo $titulo_menor ?></p>
            <h4 class="text-left">Aproveita e baixe também</h4>
            <?php if(have_rows('itens_para_download')) : while(have_rows('itens_para_download')) : the_row();

                $imagem_item        = get_sub_field('imagem_item');
                $titulo_item        = get_sub_field('titulo_item');
                $descricao_item     = get_sub_field('descricao_item');
                $link_receber       = get_sub_field('link_receber');

            ?>
                <div class="download-item">
                    <div class="row">
                        <div class="col-sm-3 thankimage">
                            <img src="<?php echo $imagem_item['url']; ?>">
                        </div><!-- col2 -->
                        <div class="col-sm-9 textthankpart">
                            <h2 class="text-left"><?php echo $titulo_item; ?></h2>
                            <p style="text-align: left;width: 60%;"><?php echo $descricao_item; ?></p>
                            <a class="receber-download" href=" <?php echo $link_receber; ?> ">RECEBER</a>
                        </div><!-- col10 -->
                    </div><!-- row -->
                </div><!-- download-item -->

            <?php endwhile; endif; ?>
        </div><!-- #content -->

        <div id="faq-sidebar">
            <div class="tit-submenu">Leia também</div>
            <ul>
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                );
                $the_query = new WP_Query( $args );
                $i = 1;

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                ?>
                <li>
                    <a href="<?php the_permalink(); ?>" class="link-img">
                        <?php
                        if ( has_post_thumbnail() ) {
                            the_post_thumbnail( 'destaque_lateral' );
                        }
                        ?>
                    </a>
                    <a href="<?php the_permalink(); ?>" class="categoria">
                        <?php
                        // Obter as categorias
                        $categories = get_the_category();
                        // Verificar se existem categorias
                        if ( ! empty( $categories ) ) {
                            // Obter a primeira categoria
                            $categorie = current($categories);
                            echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                        }
                        ?>
                    </a>
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    <div class="clearfix"></div>
                    <a href="<?php the_permalink(); ?>" class="link-ler-mais">Leia mais</a>
                </li>
                <?php $i++; endwhile; endif; wp_reset_postdata(); ?>
            </ul>

            <a href="<?php echo get_site_url(); ?>/trabalhe-conosco">
                <div class="text-center">
                    <h3 class="cta-fale">TRABALHE CONOSCO</h3>
                </div><!-- cta-fale -->
            </a>
                
        </div>


    </div><!-- #container -->

<?php get_footer(); ?>
