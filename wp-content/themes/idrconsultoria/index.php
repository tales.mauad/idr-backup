    <?php get_header(); ?>

    <!-- #main-banner -->
    <div id="main-banner">
        <div class="esq">
            <div class="field">
                <div class="txt1">BAIXE GRATUITAMENTE</div>
                <div class="txt2">O E-BOOK</div>
                <div class="txt3">Guia completo</div>
                <div class="txt4">do financiamento</div>
                <div class="txt5">Saiba como conseguir</div>
                <div class="txt6">recursos para sua empresa</div>
                <a id="bt-download" href="javascript:;"></a>
            </div>
        </div>
        <div class="dir">
            <div class="container-pergunta" data-target="#capital">
                <div class="pergunta">Quanto de capital sua empresa precisa?</div>
                <div class="opcao"><div>Até R$ 1 milhão</div></div>
                <div class="opcao active"><div>De R$ 1 a 5 milhões</div></div>
                <div class="opcao"><div>De R$ 5 a 10 milhões</div></div>
                <div class="opcao"><div>Acima de 10 milhões</div></div>
                <div class="clearfix"></div>
                <div class="error">Selecione uma opção</div>
            </div>
            <div class="container-pergunta" data-target="#faturamento">
                <div class="pergunta">Qual é o faturamento anual da sua empresa?</div>
                <div class="opcao active"><div>Até R$ 1 milhão</div></div>
                <div class="opcao"><div>De R$ 1 a 10 milhões</div></div>
                <div class="opcao"><div>De R$ 10 a 100 milhões</div></div>
                <div class="opcao"><div>Acima de 100 milhões</div></div>
                <div class="clearfix"></div>
                <div class="error">Selecione uma opção</div>
            </div>
            <div class="container-pergunta" data-target="#destino">
                <div class="pergunta">Qual é o destino do recurso?</div>
                <div class="opcao"><div>Capital de giro</div></div>
                <div class="opcao"><div>Ampliação</div></div>
                <div class="opcao"><div>Novos projetos</div></div>
                <div class="opcao active"><div>Tecnologia</div></div>
                <div class="clearfix"></div>
                <div class="error">Selecione uma opção</div>
            </div>
            <a href="javascript:;" id="main-banner-submit"></a>
        </div>
    </div>
    <!-- /#main-banner -->

    <!-- #como-funciona -->
    <div id="como-funciona" class="container">
        <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h3>COMO FUNCIONA</h3>
                <div class="hr-border"></div>
            </div>
        </div>
        <div class="row steps">
            <div id="step1" class="col-lg-3 step">
                <h2><span class="number">1.</span>Análise</h2>
                <p>Analisamos a necessidade financeira de sua empresa</p>
            </div>
            <div id="step2" class="col-lg-3 step">
                <h2><span class="number">2.</span>Documentação</h2>
                <p>Elaboramos a documentação necessária para a captação do recurso</p>
            </div>
            <div id="step3" class="col-lg-3 step">
                <h2><span class="number">3.</span>Aprovação</h2>
                <p>Acompanhamos o processo até<br /> o dinheiro cair na sua conta</p>
            </div>
            <div id="step4" class="col-lg-3">
                <h2><span class="number">4.</span>Prestação de contas</h2>
                <p>Prestamos conta do recurso captado</p>
            </div>
        </div>
        </div>
    </div>
    <!-- /#como-funciona -->

    <!-- .bg-escuro -->
    <div class="bg-escuro">
        <!-- #dicas -->
        <div id="dicas" class="container">
            <div class="row">
                <div class="col-lg-12">
                <h3>DICAS E TUTORIAIS</h3>
                <div class="hr-border"></div>
                </div>
            </div>
            <div class="row">
                <div id="dicas-desc" class="col-lg-12">
                    Leia em nosso blog dicas e tutoriais de como aprovar o seu projeto.
                    Veja notícias, editais abertos de recursos a fundo perdido e informações sobre
                    captação de financiamentos. Aprenda sobre como conseguir um capital anjo ou
                    venture capital, descubra novas oportunidades para a sua empresa e muito mais!
                </div>
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                );
                $the_query = new WP_Query( $args );
                $i = 1;
                $not_in = array();

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                    $not_in[] = $post->ID;
                ?>
                    <div class="col-lg-4">
                        <div class="dica">
                            <?php
                            if ( has_post_thumbnail() ) {
                                the_post_thumbnail( 'dicas_tutoriais' );
                            } else {
                            ?>
                                <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/dica-<?php echo $i; ?>.png" />
                            <?php
                            }
                            ?>
                            <div class="bg-dica">
                                <div class="categoria">
                                    <?php
                                    // Obter as categorias
                                    $categories = get_the_category();
                                    // Verificar se existem categorias
                                    if ( ! empty( $categories ) ) {
                                        // Obter a primeira categoria
                                        $categorie = current($categories);
                                        echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                                    }
                                    ?>
                                </div>
                                <div class="titulo">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </div>
                                <p>
                                    <?php echo $meta['texto_destaque']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php $i++; endwhile; endif; wp_reset_postdata(); ?>
            </div>
        </div>
        <!-- /#dicas -->

        <!-- #categorias -->
        <div id="categorias" class="container">
            <div id="escolha">
                <div id="escolha-label">ESCOLHA SEU ASSUNTO</div>
                <ul class="simple-tabs" id="escolha-categorias">
                    <li class="categoria-1 active">Financiamento<span class="divisor"></span></li>
                    <li class="categoria-2">Fundo Perdido<span class="divisor"></span></li>
                    <li class="categoria-3">M&A<span class="divisor"></span></li>
                    <li class="categoria-4">Lei do Bem</li>
                </ul>
            </div>
            <div class="clearfix"></div>

            <!-- .tabe-page -->
            <div id="categoria-1" class="tab-page active-page">
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                    'cat' => 3,
                    'post__not_in' => $not_in
                );
                $the_query = new WP_Query( $args );
                $i = 1;

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                ?>
                <!-- .cat-item -->
                <div class="cat-item">
                    <?php
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail( 'destaque_home' );
                    } else {
                    ?>
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-1.png" />
                    <?php
                    }
                    ?>
                    <div class="cat-item-text">
                        <div class="categoria">
                            <?php
                            // Obter as categorias
                            $categories = get_the_category();
                            // Verificar se existem categorias
                            if ( ! empty( $categories ) ) {
                                // Obter a primeira categoria
                                $categorie = current($categories);
                                echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                            }
                            ?>
                        </div>
                        <div class="titulo">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                        <div class="texto"><?php echo $meta['texto_destaque']; ?></div>
                        <a href="<?php the_permalink(); ?>">
                            <div class="link">LEIA MAIS</div>
                        </a>
                    </div>
                </div>
                <!-- /.cat-item -->
                <?php $i++; endwhile; endif; wp_reset_postdata(); ?>
            </div>
            <!-- /.tabe-page -->

            <!-- .tabe-page -->
            <div id="categoria-2" class="tab-page">
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                    'cat' => 4,
                    'post__not_in' => $not_in
                );
                $the_query = new WP_Query( $args );
                $i = 1;

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                ?>
                <!-- .cat-item -->
                <div class="cat-item">
                    <?php
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail( 'destaque_home' );
                    } else {
                    ?>
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-1.png" />
                    <?php
                    }
                    ?>
                    <div class="cat-item-text">
                        <div class="categoria">
                            <?php
                            // Obter as categorias
                            $categories = get_the_category();
                            // Verificar se existem categorias
                            if ( ! empty( $categories ) ) {
                                // Obter a primeira categoria
                                $categorie = current($categories);
                                echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                            }
                            ?>
                        </div>
                        <div class="titulo">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                        <div class="texto"><?php echo $meta['texto_destaque']; ?></div>
                        <a href="<?php the_permalink(); ?>">
                            <div class="link">LEIA MAIS</div>
                        </a>
                    </div>
                </div>
                <!-- /.cat-item -->
                <?php $i++; endwhile; endif; wp_reset_postdata(); ?>
            </div>
            <!-- /.tabe-page -->

            <!-- .tabe-page -->
            <div id="categoria-3" class="tab-page">
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                    'cat' => 6,
                    'post__not_in' => $not_in
                );
                $the_query = new WP_Query( $args );
                $i = 1;

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                ?>
                <!-- .cat-item -->
                <div class="cat-item">
                    <?php
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail( 'destaque_home' );
                    } else {
                    ?>
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-1.png" />
                    <?php
                    }
                    ?>
                    <div class="cat-item-text">
                        <div class="categoria">
                            <?php
                            // Obter as categorias
                            $categories = get_the_category();
                            // Verificar se existem categorias
                            if ( ! empty( $categories ) ) {
                                // Obter a primeira categoria
                                $categorie = current($categories);
                                echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                            }
                            ?>
                        </div>
                        <div class="titulo">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                        <div class="texto"><?php echo $meta['texto_destaque']; ?></div>
                        <a href="<?php the_permalink(); ?>">
                            <div class="link">LEIA MAIS</div>
                        </a>
                    </div>
                </div>
                <!-- /.cat-item -->
                <?php $i++; endwhile; endif; wp_reset_postdata(); ?>
            </div>
            <!-- /.tabe-page -->

            <!-- .tabe-page -->
            <div id="categoria-4" class="tab-page">
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                    'cat' => 5,
                    'post__not_in' => $not_in
                );
                $the_query = new WP_Query( $args );
                $i = 1;

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                ?>
                <!-- .cat-item -->
                <div class="cat-item">
                    <?php
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail( 'destaque_home' );
                    } else {
                    ?>
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-1.png" />
                    <?php
                    }
                    ?>
                    <div class="cat-item-text">
                        <div class="categoria">
                            <?php
                            // Obter as categorias
                            $categories = get_the_category();
                            // Verificar se existem categorias
                            if ( ! empty( $categories ) ) {
                                // Obter a primeira categoria
                                $categorie = current($categories);
                                echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                            }
                            ?>
                        </div>
                        <div class="titulo">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                        <div class="texto"><?php echo $meta['texto_destaque']; ?></div>
                        <a href="<?php the_permalink(); ?>">
                            <div class="link">LEIA MAIS</div>
                        </a>
                    </div>
                </div>
                <!-- /.cat-item -->
                <?php $i++; endwhile; endif; wp_reset_postdata(); ?>
            </div>
            <!-- /.tabe-page -->

        </div>
        <!-- /#categorias -->
    </div>
    <!-- /.bg-escuro -->

    <!-- .bg-azul -->
    <div class="bg-azul">
        <!-- #servicos -->
        <div id="servicos" class="container">
            <div class="row">
                <h3>SERVIÇOS</h3>
                <div class="hr-border"></div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="servico">
                        <h2>Fundo Perdido</h2>
                        <p>
                            A idr possui uma equipe de consultores especializados e com ampla
                            experiência na aprovação de projetos de subvenção em órgãos como FINEP,
                            FAPESP, CNPq, BNDES, etc. Estruturamos e elaboramos toda a documentação
                            necessária para aprovar o seu projeto e conseguir o recurso a fundo
                            perdido que sua empresa tanto deseja.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="servico active">
                        <h2>Lei do Bem</h2>
                        <p>
                            Reduza em até 27,2% os custos do seu projeto por meio da redução de
                            imposto de renda e CSLL. A idr dispõe de especialistas com ampla experiência
                            no auxílio à aplicação de incentivos fiscais para empresas de diversos
                            setores e tamanho.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="servico">
                        <h2>M&A</h2>
                        <p>
                            Procurando um Capital Anjo ou Venture Capital? A idr possui consultores
                            especializados em auxiliar a sua empresa a se estruturar e se aproximar dos
                            principais investidores privados do país, assessorando você durante todo o
                            processo até a obtenção do capital privado.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#servicos -->
    </div>
    <!-- .bg-azul -->

    <!-- #idr-em-numeros -->
    <div id="idr-em-numeros" class="container"></div>
    <!-- /#idr-em-numeros -->

<?php get_footer(); ?>
